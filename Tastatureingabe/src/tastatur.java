import java.util.Scanner; // Import der Klasse Scanner 
 
public class tastatur  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
//    // Neues Scanner-Objekt myScanner wird erstellt     
//    Scanner myScanner = new Scanner(System.in);  
//     
//    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
//     
//    // Die Variable zahl1 speichert die erste Eingabe 
//    int zahl1 = myScanner.nextInt();  
//     
//    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
//     
//    // Die Variable zahl2 speichert die zweite Eingabe 
//    int zahl2 = myScanner.nextInt();  
//     
//    // Die Addition der Variablen zahl1 und zahl2  
//    // wird der Variable ergebnis zugewiesen. 
//    int ergebnis = zahl1 + zahl2;  
//     
//    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
//    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);   
// 
//    myScanner.close(); 
    
//    // Neues Scanner-Objekt myScanner wird erstellt     
//    Scanner myScanner1 = new Scanner(System.in);  
//     
//    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
//    
//    // Die Variable zahl1 speichert die erste Eingabe 
//    int zahl3 = myScanner1.nextInt();  
//     
//    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
//     
//    // Die Variable zahl2 speichert die zweite Eingabe 
//    int zahl4 = myScanner1.nextInt();  
//     
//    // Die Addition der Variablen zahl1 und zahl2  
//    // wird der Variable ergebnis zugewiesen. 
//    int ergebnis1 = zahl3 * zahl4;  
//     
//    System.out.print("\n\n\nErgebnis der Multiplikation lautet: "); 
//    System.out.print(zahl3 + " * " + zahl4 + " = " + ergebnis1);   
// 
//    myScanner1.close(); 
    
    System.out.println("Hallo Nutzer");
    
  //Erzeugen eines Objekts der Klasse Scanner
  		Scanner myScanner = new Scanner(System.in);
  			
  		System.out.println("Bitte geben Sie eine ganze Zahl ein: ");
  		//Einlesen von der Konsole
  		int i = myScanner.nextInt();
  		//Ausgabe der Zahl
  		System.out.println("Sie haben " + i + " eingegeben!");
  			
  		System.out.println("Bitte geben Sie eine Zahl mit Nachkommastellen ein: ");
  		//Einlesen von der Konsole
  		float f = myScanner.nextFloat();
  		//Ausgabe der Zahl
  		System.out.println("Sie haben " + f + " eingegeben!");
  		
  		System.out.println("Bitte geben Sie eine beliebige Zeichenkette ein: ");
  		//Einlesen von der Konsole
  		String s = myScanner.next();
  		//Ausgabe der Zeichenkette
  		System.out.println("Sie haben '" + s + "' eingegeben!");
  		
  		System.out.println("Bitte geben Sie einen Wahrheitswert ein (true / false): ");
  		//Einlesen von der Konsole
  		boolean b = myScanner.nextBoolean();
  		//Ausgabe der Zeichenkette
  		System.out.println("Sie haben '" + b + "' eingegeben!");
  	
  		System.out.println("Bitte geben Sie einen einzelnen Buchstaben ein: ");
  		//Einlesen von der Konsole (Die Klasse Scanner kennt kein nextChar())
  		char c = myScanner.next().charAt(0);
  		//Ausgabe der Zeichenkette
  		System.out.println("Sie haben '" + c + "' eingegeben!");
  	}
  

    
     
  }    
