
public class Variablen {

	
		public static void main(String [] args){
		    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
		          Vereinbaren Sie eine geeignete Variable */
			
			int zaehler;

		    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
		          und geben Sie ihn auf dem Bildschirm aus.*/
			zaehler= 25;
			System.out.println("Anzahl der erfolgter Programmdurchläufe :" + zaehler);

		    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
		          eines Programms ausgewaehlt werden.
		          Vereinbaren Sie eine geeignete Variable */
			char menueauswahl;

		    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
		          und geben Sie ihn auf dem Bildschirm aus.*/
			menueauswahl= 'C';
			System.out.println("Ausgewählter Menübefehl :" + menueauswahl);

		    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
		          notwendig.
		          Vereinbaren Sie eine geeignete Variable */
			long astrozahl;

		    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
		          und geben Sie sie auf dem Bildschirm aus.*/
			astrozahl= 300000000l;
			System.out.println("Die Lichtgeschwindigkeit beträgt :" + astrozahl);

		    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
		          soll die Anzahl der Mitglieder erfasst werden.
		          Vereinbaren Sie eine geeignete Variable und initialisieren sie
		          diese sinnvoll.*/
			byte mitgliederanzahl;

		    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
			mitgliederanzahl=7;
			System.out.println("Die Anzahl der Mitglieder ist :" + mitgliederanzahl);

		    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
		          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
		          dem Bildschirm aus.*/
			double elementarleitung = 2.22;
			System.out.println("Ergebnis der Rechnung :" +elementarleitung);

		    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
		          Vereinbaren Sie eine geeignete Variable. */
			

		    /*11. Die Zahlung ist erfolgt.
		          Weisen Sie der Variable den entsprechenden Wert zu
		          und geben Sie die Variable auf dem Bildschirm aus.*/
			boolean zahlungerfolgt = true;
			System.out.println("Bezahlt? "+ zahlungerfolgt);

		  }//main
		// Variablen
	}


