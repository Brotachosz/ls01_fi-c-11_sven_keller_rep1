import java.util.Scanner;

public class MittelwertmitMethode {

	public static void main(String[] args) {
	
		double zahl1;
		double zahl2;
		double mittelwert;
		Scanner myScanner = new Scanner(System.in);
zahl1 = eingabe (myScanner,"Bitte geben Sie die erste Zahl ein: ");
zahl2 =eingabe (myScanner, "Bitte geben Sie die zweite Zahl ein: ");
mittelwert = mittelwertberechnung (zahl1,zahl2);
ausgabe(mittelwert);
myScanner.close();

	}
 public static double eingabe(Scanner myScanner, String text) {
	
	 System.out.println(text);
	 double eingabe=myScanner.nextDouble();
	 return eingabe;
 }
 public static double mittelwertberechnung(double zahl1b, double zahl2b) {
	 double m = (zahl1b + zahl2b)/ 2.0;
	 return m;
 }
 public static void ausgabe(double mittelwertergebnis) {
	 System.out.println("Mittelwert: " + mittelwertergebnis);
	 
 }
}
