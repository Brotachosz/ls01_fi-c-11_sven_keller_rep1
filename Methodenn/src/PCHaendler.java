import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
	String produkt;
	int anzahl;
	double preise;
	double mehrwert;
	double netto;
	double brutto;
	Scanner myScanner = new Scanner(System.in);
		produkt = liesString (myScanner, "was moechten Sie bestellen?");
		anzahl = liesInt (myScanner, "Geben Sie die Anzahl ein:");
		preise = liesDouble (myScanner, "Geben Sie den Nettopreis ein:");
		mehrwert = liesDouble (myScanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		netto = berechneGesamtnettopreis (anzahl, preise);
		brutto = berechneGesamtbruttopreis (netto, 
		mehrwert);
		 
	    rechungausgeben( produkt, anzahl, netto, brutto, mehrwert);
	}

		// Benutzereingaben lesen
		public static String liesString(Scanner myScanner, String text) {
		
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel; 
		}
		public static int liesInt(Scanner myScanner, String zahl) {
		System.out.println(zahl);
		int anzahl = myScanner.nextInt();
		return anzahl;
		}
		public static double liesDouble(Scanner myScanner, String preise) {
		System.out.println(preise);
		double preis = myScanner.nextDouble();
		return preis;
		}

		// Verarbeiten
		public static double berechneGesamtnettopreis( int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
		}
		public static double berechneGesamtbruttopreis(double preise, 
				double mehrwert) {
		double bruttogesamtpreis = preise * (1 + mehrwert / 100);
		return bruttogesamtpreis;
		}
		// Ausgeben
		public static void rechungausgeben(String produkt, int anzahl, double 
				netto, double brutto, 
				   double mehrwert) {

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", produkt, anzahl, netto);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", produkt, anzahl, brutto, mehrwert, "%");
		
		}
}

